package pl.com.horus.golonko;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MyStructureTest {
    private IMyStructure myStructure;

    private IMyStructure myStructureComposite;

    private List<INode> nodes;

    @Before
    public void setUp() {
        nodes = Arrays.asList(
            new NodeTest("00", "xx"),
            new NodeTest("01", "xy"),
            new NodeTest(null, null)
        );
        myStructure = new MyStructure(nodes);
        myStructureComposite = new MyStructure(new CompositeNodeTest(nodes));
    }

    @Test
    public void testFindByCodeIsFound() {
        assertEquals(nodes.get(0), myStructure.findByCode("00"));
        assertEquals(nodes.get(1), myStructure.findByCode("01"));
        assertEquals(nodes.get(2), myStructure.findByCode(null));

        assertEquals(nodes.get(0), myStructureComposite.findByCode("00"));
        assertEquals(nodes.get(1), myStructureComposite.findByCode("01"));
        assertEquals(nodes.get(2), myStructureComposite.findByCode(null));
    }

    @Test
    public void testFindByCodeIsNotFound() {
        assertNull(myStructure.findByCode("02"));

        assertNull(myStructureComposite.findByCode("02"));
    }

    @Test
    public void testFindByRendererIsFound() {
        assertEquals(nodes.get(0), myStructure.findByRenderer("xx"));
        assertEquals(nodes.get(1), myStructure.findByRenderer("xy"));
        assertEquals(nodes.get(2), myStructure.findByRenderer(null));

        assertEquals(nodes.get(0), myStructureComposite.findByRenderer("xx"));
        assertEquals(nodes.get(1), myStructureComposite.findByRenderer("xy"));
        assertEquals(nodes.get(2), myStructureComposite.findByRenderer(null));
    }

    @Test
    public void testFindByRendererIsNotFound() {
        assertNull(myStructure.findByRenderer("xz"));

        assertNull(myStructureComposite.findByRenderer("xz"));
    }

    @Test
    public void testCount() {
        assertEquals(nodes.size(), myStructure.count());

        assertEquals(nodes.size(), myStructureComposite.count());
    }

    private final static class CompositeNodeTest implements ICompositeNode {
        private final List<INode> nodes;

        private CompositeNodeTest(List<INode> nodes) {
            this.nodes = nodes;
        }

        @Override public List<INode> getNodes() {
            return this.nodes;
        }

        @Override public String getCode() {
            return this.nodes.stream()
                             .map(INode::getCode)
                             .collect(Collectors.joining(", "));
        }

        @Override public String getRenderer() {
            return this.nodes.stream()
                             .map(INode::getRenderer)
                             .collect(Collectors.joining(", "));
        }
    }

    private final static class NodeTest implements INode {
        private final String code;

        private final String renderer;

        private NodeTest(String code, String renderer) {
            this.code = code;
            this.renderer = renderer;
        }

        @Override public String getCode() {
            return this.code;
        }

        @Override public String getRenderer() {
            return this.renderer;
        }
    }
}