package pl.com.horus.golonko;

import java.util.List;

interface ICompositeNode extends INode {
    List<INode> getNodes();
}
