package pl.com.horus.golonko;

public interface INode {
    String getCode();

    String getRenderer();
}
