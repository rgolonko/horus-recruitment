package pl.com.horus.golonko;

import java.util.List;
import java.util.function.Predicate;

/**
 * Poniżej przekazujemy zadanie z prośbą o analizę poniższego kodu i zaimplementowanie metod findByCode, findByRenderer, count w klasie MyStructure -
 * najchętniej unikając powielania kodu i umieszczając całą logikę w klasie MyStructure. Z uwzględnieniem w analizie i implementacji interfejs ICompositeNode!
 */
public class MyStructure implements IMyStructure {
    private final List<INode> nodes;

    // Założyłem, że MyStructure będzie przechowywać immutowalną referencję listy nodes, która nie może zawierać wartości null
    public MyStructure(List<INode> nodes) {
        this.nodes = List.copyOf(nodes);
    }

    public MyStructure(ICompositeNode compositeNode) {
        this(compositeNode.getNodes());
    }

    @Override
    public INode findByCode(String code) {
        return findByCondition(node -> equalsStrings(code, node.getCode()));
    }

    @Override
    public INode findByRenderer(String renderer) {
        return findByCondition(node -> equalsStrings(renderer, node.getRenderer()));
    }

    @Override
    public int count() {
        return this.nodes.size();
    }

    private INode findByCondition(Predicate<INode> conditionFilter) {
        return this.nodes.stream()
                         .filter(conditionFilter)
                         .findFirst()
                         .orElse(null);
    }

    /*
    Dobrą praktyką byłoby przeniesienie poniższej metody do pomocniczej klasy np. StringUtils
    Stworzyłem metodę equalsStrings w założeniu, że pola klas implementującej INode oraz
    parametry przekazywane do metod findByCode oraz findByRenderer mogą być null
    */
    private boolean equalsStrings(String str1, String str2) {
        if (str1 == null && str2 == null) {
            return true;
        }
        if (str1 != null) {
            return str1.equals(str2);
        }
        return false;
    }
}
